local composer = require( "composer" )
 
local scene = composer.newScene()
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 
 local appBackground
 local title
 local btnStartTestExample

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
local function sceneTestExample ( event )

   composer.gotoScene( "scenes.testExample" )

end    

-- create()
function scene:create( event )
 
    local sceneGroup = self.view
    
    appBackground   = display.newImageRect( "images/fon.png" , display.contentWidth , display.contentHeight/.8 )
    appBackground.x = display.contentCenterX
    appBackground.y = display.contentCenterY
    sceneGroup:insert(appBackground)

    title   = display.newImageRect( "images/test.png" , display.contentWidth*.6 , display.contentHeight*.3 )
    title.x = display.contentCenterX
    title.y = display.contentCenterY*.9
    sceneGroup:insert(title)

    btnStartTestExample   = display.newImageRect( "images/push_me.png" , display.contentWidth*.7 , display.contentHeight*.15 )
    btnStartTestExample.x = display.contentCenterX
    btnStartTestExample.y = display.contentCenterY/.55
    sceneGroup:insert(btnStartTestExample)

    btnStartTestExample:addEventListener("tap", sceneTestExample)

 end
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 

    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene