display.setStatusBar( display.HiddenStatusBar )

local widget   = require( "widget" )
local composer = require( "composer" )
local json     = require( "json")

local scene = composer.newScene()

local appBackground, btnBackMenu
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local scrollView = widget.newScrollView{
    width                    = display.contentWidth,
    height                   = display.contentHeight - 100,
    scrollWidth              = 0,
    scrollHeight             = 0,
    horizontalScrollDisabled = true,
    hideBackground           = true
}


local function loadData( filename )
    local path          = system.pathForFile(filename)
    local contents      = ""
    local urlDataTable  = {}
    local file          = io.open(path, "r")

    if (file) then 
        local contents = file:read( "*a" )
        urlDataTable   = json.decode(contents) 
        io.close(file)
        return urlDataTable
    end

    return nil

end


local function networkListener( event )
    if ( event.isError ) then
        print ( "Network error - download failed" )
    else
        event.target.width  = display.contentWidth-display.contentWidth/6
        event.target.height = display.contentWidth-display.contentWidth/3
        scrollView:insert(event.target)
        print ( "RESPONSE: ", event.response.filename )

    end
end

local function backMenu( event )
        composer.gotoScene( "scenes.menu" )
    end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- ----------------------------------------------------------------------------------- 

-- create()
function scene:create( event )
 
    local sceneGroup = self.view
    
    appBackground   = display.newImageRect("images/fon.png", display.contentHeight, display.contentHeight/.8 )
    appBackground.x = display.contentCenterX
    appBackground.y = display.contentCenterY
    sceneGroup:insert(appBackground)
    
    local httpData = loadData("datas/images.json");

    for i = 1, #httpData.images do
        display.loadRemoteImage(  
            httpData.images[i], 
            "GET", 
            networkListener, 
            tostring(i)..".png", 
            system.TemporaryDirectory,
            display.contentCenterX,
            display.contentCenterX+display.contentCenterX*1.5*(i-1)
        )
    end

    sceneGroup:insert(scrollView)

    btnBackMenu   = display.newImageRect("images/back.png",display.contentWidth*.7 , display.contentHeight*.18 )
    btnBackMenu.x = display.contentCenterX
    btnBackMenu.y = display.contentCenterY/.55
    sceneGroup:insert(btnBackMenu)
    btnBackMenu:addEventListener( "tap", backMenu )

end
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
        
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create",  scene )
scene:addEventListener( "show",    scene )
scene:addEventListener( "hide",    scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene